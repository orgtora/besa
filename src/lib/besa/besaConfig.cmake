# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include(CMakePushCheckState)
cmake_push_check_state()

set(BESA_BINARY_PATH_ARRAY ${CMAKE_SYSTEM_PREFIX_PATH})
list(PREPEND BESA_BINARY_PATH_ARRAY ${CMAKE_PREFIX_PATH})
list(TRANSFORM BESA_BINARY_PATH_ARRAY APPEND "/bin")

foreach(component IN LISTS want_components)
  include(${CMAKE_CURRENT_LIST_DIR}/${component}.cmake)
endforeach()

include(${CMAKE_CURRENT_LIST_DIR}/asan.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/builder.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/format.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/tidy.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/coverage.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/doxygen.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/explorer.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/feature-set.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/lsan.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/mode.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/surrogate.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/test.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ubsan.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/version.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/warning.all.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/warning.deprecated.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/warning.error.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/warning.essential.cmake)


cmake_pop_check_state()
